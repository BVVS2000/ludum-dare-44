﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class LoadAmbientVolume : MonoBehaviour
{
    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if(FindObjectOfType<Menu>())
        {
            source.volume = FindObjectOfType<Menu>().vol;
        }
    }
}
