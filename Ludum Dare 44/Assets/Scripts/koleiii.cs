﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class koleiii : MonoBehaviour
{
    public GameObject CanCan1;
    public GameObject CanCan2;
    public GameObject CanCan3;
    public GameObject CanCan4;
    public GameObject CanCan5;
    public GameObject CanCan6;
    void Start()
    {
        StartCoroutine(Example());
        IEnumerator Example()
        {
            CanCan1.SetActive(true);
            yield return new WaitForSeconds(8);
            CanCan1.SetActive(false);
            CanCan2.SetActive(true);
            yield return new WaitForSeconds(8);
            CanCan2.SetActive(false);
            CanCan3.SetActive(true);
            yield return new WaitForSeconds(8);
            CanCan3.SetActive(false);
            CanCan4.SetActive(true);
            yield return new WaitForSeconds(8);
            CanCan4.SetActive(false);
            CanCan5.SetActive(true);
            yield return new WaitForSeconds(8);
            CanCan5.SetActive(false);
            CanCan6.SetActive(true);
        }
    }
}
