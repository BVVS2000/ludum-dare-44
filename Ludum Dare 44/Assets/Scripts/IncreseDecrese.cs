﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreseDecrese : MonoBehaviour
{
    public void Do()
    {
        FindObjectOfType<GameManager>().IncreseDecrese();
    }
}
