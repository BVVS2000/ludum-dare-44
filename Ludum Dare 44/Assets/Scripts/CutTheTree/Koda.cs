﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Koda : MonoBehaviour
{
    public int hp = 10;
    public float minDistance = 0.5f;

    private AudioSource source;

    public void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(Vector2.Distance((Vector2)transform.position, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition)) <= minDistance)
            {
                source = GetComponent<AudioSource>();
                if (FindObjectOfType<Menu>())
                {
                    source.volume = FindObjectOfType<Menu>().vol;
                }
                source.Play();
                if (--hp <= 0)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
      
}
