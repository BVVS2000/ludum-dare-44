﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseShake : MonoBehaviour
{
    public Transform pointer;
    public float maxDistance = 3f;

    public float speed = 1f;

    private Vector2 dir;

    private void Start()
    {
        dir = (new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f))).normalized * speed * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
        
        if(Vector2.Distance(Vector2.zero, (Vector2)pointer.localPosition + dir) <= maxDistance)
        {
            pointer.Translate(dir);
        }
        else
        {
            dir = (new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f))).normalized * speed * Time.deltaTime;
        }
    }
}
