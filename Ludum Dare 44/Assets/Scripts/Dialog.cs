﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public enum AfterDialog { Minigame, Quiz, Apteka }
public enum DialogState { Normal, AfterWin, AfterLose, NoHelp }

[Serializable]
public class Dialog : MonoBehaviour
{
    public int issueId = 0;

    public string characterName = "";

    public List<string> Dialogs;
    public List<string> DialogsAfterWin;
    public List<string> DialogsAfterLose;
    public List<string> DialogsIfNoHelp;

    public Text dialogText;

    public AfterDialog afterDialog = AfterDialog.Quiz;
    public DialogState dialogState = DialogState.Normal;
    public Quiz quiz;
    public string minigameName = "";

    public AudioSource source;

    public RectTransform choose;

    private ShowQuiz showQuiz;

    public List<GameObject> objsToHide = new List<GameObject>();
    public GameObject background;

    private void Start()
    {
        source = GetComponent<AudioSource>();
        source.volume = FindObjectOfType<Menu>().vol;

        background = GameObject.FindGameObjectWithTag("Background");
        showQuiz = FindObjectOfType<ShowQuiz>();
        Show();
    }

    private void LateUpdate()
    {
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return) || Input.GetMouseButtonDown(0)) && !choose.gameObject.activeInHierarchy && dialogText.gameObject.activeInHierarchy)
        {
            switch (dialogState)
            {
                case DialogState.Normal:
                    {
                        if (issueId < Dialogs.Count)
                        {
                            dialogText.text = characterName + ": " + Dialogs[issueId++];
                        }
                        else
                        {
                            choose.gameObject.SetActive(true);
                        }

                        break;
                    }
                case DialogState.NoHelp:
                    {
                        if (issueId < DialogsIfNoHelp.Count)
                        {
                            dialogText.text = characterName + ": " + DialogsIfNoHelp[issueId++];
                        }
                        else
                        {
                            Hide(false);
                        }

                        break;
                    }
                case DialogState.AfterLose:
                    {
                        if (issueId < DialogsAfterLose.Count)
                        {
                            dialogText.text = characterName + ": " + Dialogs[issueId++];
                        }
                        else
                        {
                            Hide(false);
                        }

                        break;
                    }
                case DialogState.AfterWin:
                    {
                        if (issueId < DialogsAfterWin.Count)
                        {
                            dialogText.text = characterName + ": " + DialogsAfterWin[issueId++];
                        }
                        else
                        {
                            Hide(false);
                        }

                        break;
                    }
            }
        }
    }

    public void SetState(DialogState state)
    {
        dialogState = state;
        issueId = 0;
        
        if(state == DialogState.NoHelp)
        {
            FindObjectOfType<GameManager>().IncreseDecrese();
        }
    }

    public void SetState(int state)
    {
        dialogState = (DialogState)state;
        issueId = 0;

        if ((DialogState)state == DialogState.NoHelp)
        {
            FindObjectOfType<GameManager>().IncreseDecrese();
        }

    }

    public void Show()
    {
        Restart();

        foreach (GameObject obj in objsToHide)
        {
            obj.SetActive(true);
        }
        background.SetActive(true);
        switch (dialogState)
        {
            case DialogState.Normal:
                {
                    if (issueId < Dialogs.Count)
                    {
                        if (issueId == 0)
                        {
                            source.Play();
                        }

                        dialogText.text = characterName + ": " + Dialogs[issueId++];
                    }
                    else
                    {
                        choose.gameObject.SetActive(true);
                    }

                    break;
                }
            case DialogState.NoHelp:
                {
                    if (issueId < DialogsIfNoHelp.Count)
                    {
                        dialogText.text = characterName + ": " + DialogsIfNoHelp[issueId++];
                    }
                    else
                    {
                        Hide(false);
                    }

                    break;
                }
            case DialogState.AfterLose:
                {
                    if (issueId < DialogsAfterLose.Count)
                    {
                        dialogText.text = characterName + ": " + DialogsAfterLose[issueId++];
                    }
                    else
                    {
                        Hide(false);
                    }

                    break;
                }
            case DialogState.AfterWin:
                {
                    if (issueId < DialogsAfterWin.Count)
                    {
                        dialogText.text = characterName + ": " + DialogsAfterWin[issueId++];
                    }
                    else
                    {
                        Hide(false);
                    }

                    break;
                }
        }
    }

    public void Hide(bool withMinigame)
    {
        if(withMinigame)
        {
            if(afterDialog == AfterDialog.Quiz)
            {
                background.SetActive(false);
                showQuiz.Show(quiz);
            }
            else if(afterDialog == AfterDialog.Minigame)
            {
                SceneManager.LoadScene(minigameName);
            }
            else if(afterDialog == AfterDialog.Apteka)
            {
                GameManager gameManager = FindObjectOfType<GameManager>();

                gameManager.GiveMed();
                SetState(DialogState.AfterWin);
                

                choose.gameObject.SetActive(false);
                Show();

                Debug.Log("Shoulds works");

                return;
            }
            else
            {
                Debug.Log("Whats wrong");
            }
        }
        else
        {
            FindObjectOfType<GameManager>().NextDialog();
        }

        foreach(GameObject obj in objsToHide)
        {
            obj.SetActive(false);
        }

        choose.gameObject.SetActive(false);
        
    }

    public void Restart()
    {
        dialogText.text = "";
        issueId = 0;
    }
}
