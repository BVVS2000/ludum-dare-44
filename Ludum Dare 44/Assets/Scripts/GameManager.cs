﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public partial class GameManager : MonoBehaviour
{
    public List<Dialog> dialogs = new List<Dialog>();
    // Zmienna na cutscenki ALE POTRZEBNE SĄ CUTSCENKI

    public int dialogId = -1;

    public Dialog actDialog;

    public bool inMinigame = false;
    public bool win = false;

    public Dialog aptekorz;
    public bool aptekorzApperead = false;

    public AudioSource ambient;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        SceneManager.LoadScene("quiz");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public void NextDialog()
    {

        if (actDialog != null)
        {
            actDialog.gameObject.SetActive(false);
        }
        if (money != maxMoney || aptekorzApperead)
        {
            if (dialogId < dialogs.Count - 1)
            {
                // Do dodania cutsceneki między dialogami
                actDialog = Instantiate(dialogs[++dialogId].gameObject, Vector3.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("Respawn").transform).GetComponent<Dialog>();
                aptekorzApperead = false;
            }
            else
            {
                SceneManager.LoadScene("KUNIEC");
            }
        }
        else
        {
            actDialog = Instantiate(aptekorz.gameObject, Vector3.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("Respawn").transform).GetComponent<Dialog>();
            aptekorzApperead = true;
        }
    }

    public void ExitMinigame(bool win)
    {
        if (win)
        {
            actDialog.SetState(DialogState.AfterWin);
        }
        else
        {
            actDialog.SetState(DialogState.AfterLose);
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("quiz"))
        {
            actDialog.Show();
        }
    }

    public void ExitMinigame()
    {
        inMinigame = false;

        if(win)
        {
            actDialog.SetState(DialogState.AfterWin);
        }
        else
        {
            actDialog.SetState(DialogState.AfterLose);
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene == SceneManager.GetSceneByName("quiz"))
        {
            ambient.UnPause();
            if(inMinigame)
            {
                actDialog = Instantiate(dialogs[dialogId].gameObject, Vector3.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("Respawn").transform).GetComponent<Dialog>();
                ExitMinigame();
            }
            else
            {
                NextDialog();
            }
        }
        else
        {
            ambient.Pause();
            if(FindObjectOfType<Minigame>())
            {
                inMinigame = true;
            }
        }
    }
}
