﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMinigame : MonoBehaviour
{
    public GameObject choose;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(choose.activeInHierarchy)
            {
                choose.SetActive(false);
            }else
            {
                choose.SetActive(true);
            }
        }
    }
}
