﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitThrower : MonoBehaviour
{
    public List<Transform> positions;

    public float minThrowDelay = 1;
    public float maxThrowDelay = 2;
    
    [HideInInspector]
    public float toThrow = 0.0f;

    public List<GameObject> fruits;

    public GameObject tutorial;

    private void Update()
    {
        if (!tutorial.activeInHierarchy && !FindObjectOfType<PauseMinigame>().choose.activeInHierarchy)
        {
            if (toThrow > 0.0f)
            {
                toThrow -= Time.deltaTime;
            }

            if (toThrow <= 0.0f)
            {
                Throw(fruits[Random.Range(0, fruits.Count)], positions[Random.Range(0, positions.Count)].position);
            }
        }
    }

    public void Throw(GameObject fruit, Vector3 pos)
    {
        Instantiate(fruit, pos, Quaternion.identity);
        toThrow = Random.Range(minThrowDelay, maxThrowDelay);
    }
}
