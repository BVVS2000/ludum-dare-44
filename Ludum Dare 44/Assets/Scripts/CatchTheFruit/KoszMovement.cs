﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class KoszMovement : MonoBehaviour
{
    public float speed = 1f;

    private Rigidbody2D rb;
    private float x;

    public GameObject tutorial;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        x = Input.GetAxisRaw("Horizontal");
    }

    private void FixedUpdate()
    {
        if (!tutorial.activeInHierarchy && !FindObjectOfType<PauseMinigame>().choose.activeInHierarchy)
        {
            rb.MovePosition((Vector2)transform.position + Vector2.right * x * speed * Time.deltaTime);
        }
    }
}
