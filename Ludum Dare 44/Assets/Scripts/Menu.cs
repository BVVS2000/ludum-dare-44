﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public AudioSource Source;
    public AudioSource Creditss;
    public GameObject CanCan1;
    public GameObject CanCan2;
    public GameObject CanCan3;
    public GameObject CanCan4;
    public GameObject CanCan5;
    public GameObject CanCan6;
    public GameObject CanCan7;
    public GameObject CanCan8;
    public GameObject x1;
    public GameObject x2;
    public GameObject x3;
    public GameObject x4;

    public bool PLAYDOING = false;
    public int x;
    public bool Space = false;

    public Slider Vol;

    public float vol;

    public void Start()
    {
        if (Vol)
        {
            Vol.value = 0.5f;
        }
        Creditss.Stop();
    }


    public void Update()
    {
        if (Vol)
        {
            Source.volume = Vol.value;
            Creditss.volume = Vol.value;
            vol = Vol.value;
        }
        if (PLAYDOING == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                PlayPlay();
                x = 15;
                Space = true;
            }
        }
    }

    public void Play()
    {
        PLAYDOING = true;

        StartCoroutine(Example());
        IEnumerator Example()
        {
            x1.SetActive(false);
            x2.SetActive(false);
            x3.SetActive(false);
            x4.SetActive(false);
            CanCan2.SetActive(false);
            CanCan1.SetActive(false);
            CanCan4.SetActive(true);
            yield return new WaitForSeconds(x);
            CanCan4.SetActive(false);
            CanCan5.SetActive(true);
            yield return new WaitForSeconds(x);
            CanCan5.SetActive(false);
            CanCan6.SetActive(true);
            yield return new WaitForSeconds(x);
            CanCan6.SetActive(false);
            CanCan7.SetActive(true);
            yield return new WaitForSeconds(x);
            CanCan7.SetActive(false);
            CanCan3.SetActive(true);
            yield return new WaitForSeconds(5);
            SceneManager.LoadScene("Index");
        }

    }
    public void Options()
    {
        CanCan1.SetActive(false);
        CanCan2.SetActive(true);
    }
    public void Back()
    {
        CanCan1.SetActive(true);
        CanCan2.SetActive(false);
        CanCan8.SetActive(false);
        Source.UnPause();
        Creditss.Stop();

    }
    public void Credits()
    {
        Creditss.Play();
        Source.Pause();
        CanCan1.SetActive(false);
        CanCan8.SetActive(true);
    }
    public void Exit()
    {
        Application.Quit();
    }

    public void PlayPlay()
    {

        StartCoroutine(ExampleEx());
        IEnumerator ExampleEx()
        {
            CanCan1.SetActive(false);
            CanCan2.SetActive(false);
            CanCan5.SetActive(false);
            CanCan4.SetActive(false);
            CanCan6.SetActive(false);
            CanCan7.SetActive(false);
            CanCan8.SetActive(false);
            CanCan3.SetActive(true);
            yield return new WaitForSeconds(5);
            SceneManager.LoadScene("Index");
        }

    }
}
