﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatchFruit : Minigame
{
    public int goal = 20;
    public int points = 0;

    public Text catchedFruits;

    private AudioSource source;

    private void Start()
    {
        catchedFruits.text = "Fruits: " + points;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!FindObjectOfType<PauseMinigame>().choose.activeInHierarchy)
        {
            if (collision.CompareTag("Fruit"))
            {
                catchedFruits.text = "Fruits: " + ++points;

                source = GetComponent<AudioSource>();
                if (FindObjectOfType<Menu>())
                {
                    source.volume = FindObjectOfType<Menu>().vol;
                }
                source.Play();

                if (points == goal)
                {
                    Win();
                }

                Destroy(collision.gameObject);
            }
        }
    }
}
