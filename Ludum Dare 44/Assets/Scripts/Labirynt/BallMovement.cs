﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BallMovement : MonoBehaviour
{
    public float speed;

    private Rigidbody2D rb;

    float x = 0;
    float y = 0;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");
    }

    private void FixedUpdate()
    {
        if (!FindObjectOfType<PauseMinigame>().choose.activeInHierarchy)
        {
            rb.MovePosition((Vector2)transform.position + new Vector2(x, y) * speed * Time.deltaTime);
        }
    }
}
