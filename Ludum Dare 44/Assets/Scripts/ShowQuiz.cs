﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowQuiz : MonoBehaviour
{
    public Text question;
    public List<Text> answers;

    private Quiz quiz;

    public AudioSource good;
    public AudioSource bad;

    public void Show(Quiz quiz)
    {
        this.quiz = quiz;
        Restart();

        question.gameObject.SetActive(true);
        foreach (Text t in answers)
        {
            t.gameObject.SetActive(true);
        }

        question.text = quiz.question;

        {
            int i = 0;
            foreach(Text t in answers)
            {
                t.text = quiz.answers[i++];
            }
        }
    }

    public void Reply(int ans)
    {
        if (ans == quiz.correctAnswer)
        {
            if (FindObjectOfType<Menu>())
            {
                good.volume = FindObjectOfType<Menu>().vol;
            }
            good.Play();

            FindObjectOfType<GameManager>().GiveReward(quiz.reward);
            Hide(true);
        }
        else
        {
            if (FindObjectOfType<Menu>())
            {
                bad.volume = FindObjectOfType<Menu>().vol;
            }
            bad.Play();

            Hide(false);
        }
    }

    public void Hide(bool win)
    {
        question.gameObject.SetActive(false);
        foreach(Text t in answers)
        {
            t.gameObject.SetActive(false);
        }
        
        FindObjectOfType<GameManager>().ExitMinigame(win);
    }

    public void Restart()
    {
        question.text = "";
        foreach(Text t in answers)
        {
            t.text = "";
        }
    }
}
