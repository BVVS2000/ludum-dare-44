﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class CutTheTree : Minigame
{
    public int points = 0;

    public List<Koda> kodes = new List<Koda>();

    void Update()
    {
        if(!kodes.Where(t => t.gameObject.activeInHierarchy).FirstOrDefault())
        {
            Win();
        }
    }
}
