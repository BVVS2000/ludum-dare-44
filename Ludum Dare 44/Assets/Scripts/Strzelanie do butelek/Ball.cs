﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Ball : MonoBehaviour
{
    public float resizeSpeed = 0.3f;
    public float minSize = 0.2f;

    AudioSource source;

    private void Update()
    {
        if (transform.localScale.x > minSize)
        {
            transform.localScale -= Vector3.one * resizeSpeed * Time.deltaTime;
        }
        else if(transform.localScale.x <= minSize)
        {
            transform.localScale = Vector3.one * minSize;

            Bottle bottle = FindObjectsOfType<Bottle>().Where(t => Vector2.Distance(transform.position, t.transform.position) <= 0.6).FirstOrDefault();
            if(bottle)
            {
                bottle.Shot();
            }
            else
            {
                source = GetComponent<AudioSource>();
                if (FindObjectOfType<Menu>())
                {
                    source.volume = FindObjectOfType<Menu>().vol;
                }
                source.Play();
            }

            FindObjectOfType<BottleShot>().Check();

            Destroy(gameObject);
        }
    }
}
