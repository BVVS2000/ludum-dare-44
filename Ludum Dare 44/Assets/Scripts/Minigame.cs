﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Minigame : MonoBehaviour
{
    public int reward;

    public void Win()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();

        gameManager.win = true;
        gameManager.GiveReward(reward);
        
        SceneManager.LoadScene("quiz");
    }

    public void Lose()
    {
        FindObjectOfType<GameManager>().win = false;
        SceneManager.LoadScene("quiz");
    }
}
