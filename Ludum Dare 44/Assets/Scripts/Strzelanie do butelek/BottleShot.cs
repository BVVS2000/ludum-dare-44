﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BottleShot : Minigame
{
    public ThrowBall thrower;
    public List<Bottle> bottles;

    public void Start()
    {
        bottles = FindObjectsOfType<Bottle>().ToList();
    }

    public void Check()
    {
        List<Bottle> unshooted = bottles.Where(t => t.shooted == false).ToList();

        if(unshooted.Count == 0)
        {
            Win();
        }
        else if(thrower.approaches == 0)
        {
            Lose();
        }
    }
}
