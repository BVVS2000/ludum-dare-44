﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    AudioSource source;

    float time = 5;

    void Start()
    {
        source = GetComponent<AudioSource>();
        source.volume = FindObjectOfType<Menu>().vol;
        Destroy(FindObjectOfType<GameManager>().gameObject);
        Destroy(FindObjectOfType<Menu>().gameObject);
    }

    private void Update()
    {
        if(time>0)
        {
            time -= Time.deltaTime;
        }
        else
        {
            SceneManager.LoadScene("menu");
        }
    }
}
