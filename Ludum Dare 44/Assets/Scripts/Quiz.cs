﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quiz : MonoBehaviour
{
    public string question;
    public List<string> answers = new List<string>();
    public int correctAnswer = 1;
    public int reward;
}
