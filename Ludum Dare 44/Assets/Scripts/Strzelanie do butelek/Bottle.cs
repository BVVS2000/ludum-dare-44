﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottle : MonoBehaviour
{
    public bool shooted = false;

    private AudioSource source;

    public void Shot()
    {
        source = GetComponent<AudioSource>();
        if (FindObjectOfType<Menu>())
        {
            source.volume = FindObjectOfType<Menu>().vol;
        }
        source.Play();

        shooted = true;
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
