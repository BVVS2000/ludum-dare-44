﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThrowBall : MonoBehaviour
{
    public Ball ball;
    public int approaches = 5;

    public float shotDelay = 1;
    private float toShot = 0.0f;

    public Transform pointOfShot;

    public Text ballCount;

    public GameObject tutorial;

    private AudioSource source;

    void Update()
    {
        if (!FindObjectOfType<PauseMinigame>().choose.activeInHierarchy && !tutorial.activeInHierarchy)
        {
            if (toShot > 0.0f)
            {
                toShot -= Time.deltaTime;
            }
            else
            {
                if (Input.GetMouseButtonDown(0) && approaches > 0)
                {
                    source = GetComponent<AudioSource>();
                    if (FindObjectOfType<Menu>())
                    {
                        source.volume = FindObjectOfType<Menu>().vol;
                    }
                    source.Play();

                    Instantiate(ball.gameObject, pointOfShot.position, Quaternion.identity);
                    toShot = shotDelay;
                    approaches--;
                }
            }
        }

        ballCount.text = "Balls: " + approaches;
    }
}
