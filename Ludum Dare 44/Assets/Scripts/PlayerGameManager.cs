﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public partial class GameManager : MonoBehaviour
{
    [Header("Player")]
    public Slider healthSlider;
    public Slider moneyText;

    public int money = 0;
    public int maxMoney = 2;
    public float health = 100;

    public float healthDecrese;
    public float healthDecreseIncrese;
    public int medsCost = 2;

    AudioSource source;

    private void Update()
    {
        if(FindObjectOfType<PauseMinigame>() || GameObject.FindGameObjectWithTag("Tutorial"))
        {
            bool can = true;
            if (FindObjectOfType<PauseMinigame>() && FindObjectOfType<PauseMinigame>().choose.activeInHierarchy)
            {
                can = false;
            }

            if (GameObject.FindGameObjectWithTag("Tutorial") && GameObject.FindGameObjectWithTag("Tutorial").activeInHierarchy)
            {
                can = false;
            }

            if (can)
            {
                health -= healthDecrese * Time.deltaTime;
            }
        }
        else
        {
            health -= healthDecrese * Time.deltaTime;
        }

        if (health <= 0.0f)
        {
            SceneManager.LoadScene("GameOver");
        }
        else if(health <= 25f)
        {
            source = GetComponent<AudioSource>();

            if (FindObjectOfType<Menu>())
            {
                source.volume = FindObjectOfType<Menu>().vol;
            }

            if(!source.isPlaying) 
            {
                source.Play();
            }
        }else
        {
            source = GetComponent<AudioSource>();
            source.Stop();
        }
    }

    private void LateUpdate()
    {
        Refresh();
    }

    public void IncreseDecrese()
    {
        healthDecrese += healthDecreseIncrese;
    }

    public void GiveReward(int reward)
    {
        money += reward;
    }

    public void GiveMed()
    {
        health = 100;
        money = 0;
    }

    public void Refresh()
    {
        if (healthSlider)
        {
            healthSlider.value = health / 100;
        }

        if (moneyText)
        {
            moneyText.value = (float)money / maxMoney;
        }
    }
}
